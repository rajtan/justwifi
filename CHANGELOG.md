# JustWifi change log

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Fixed
- Remove declaration from example, jw is a singleton

## [1.1.0] 2016-09-29
- Initial working version as standalone library
